

let countTimer= document.querySelector('.countdown-timer');
let countTimerValues = {
    days:    countTimer.querySelector('.countdown-timer__value-days'),
    hours:   countTimer.querySelector('.countdown-timer__value-hours'),
    minutes: countTimer.querySelector('.countdown-timer__value-minutes'),
    seconds: countTimer.querySelector('.countdown-timer__value-seconds')
};


let start = countTimer.dataset.start? new Date(countTimer.dataset.start).getTime() : new Date().getTime();
let end   = countTimer.dataset.end? new Date(countTimer.dataset.end).getTime() : new Date().getTime();


if (start < end) {
    let x = setInterval(() => {
        let diff = end - start;

        if (diff < 0) {
            diff = 0;
            clearInterval(x);
        }

        countTimerValues.days.innerHTML    = (Math.floor(diff / MILLISECONDS_IN_DAY)).zeros();
        countTimerValues.hours.innerHTML   = (Math.floor((diff % MILLISECONDS_IN_DAY) / MILLISECONDS_IN_HOUR)).zeros();
        countTimerValues.minutes.innerHTML = (Math.floor((diff % MILLISECONDS_IN_HOUR) / MILLISECONDS_IN_MINUTE)).zeros();
        countTimerValues.seconds.innerHTML = (Math.floor((diff % MILLISECONDS_IN_MINUTE) / MILLISECONDS)).zeros();

        end = end - 1000;

    }, 1000);
}




